let API = {
    URI: (feature) => `https://api.openweathermap.org/data/2.5/${feature}?`,
    KEY: "63265b3e4e30f9c2fc312c8e843493ed",
    IMG: (id) => { return `https://openweathermap.org/img/wn/${id}@2x.png` }
}


let cache = {
    data: {},
}




const temperatureElement = {
    type: "K",

    defaultElements: ["max-temp", "min-temp", "temp"],
    generatedElements: [],
    getElements: () => {
        return temperatureElement.defaultElements.concat(
            temperatureElement.generatedElements
        )
    },

    clearGenerated: ()=>{temperatureElement.generatedElements = []}




}




const createLocationQueryURI = (locationName) => {

    return `${API.URI("weather")}`
        + `q=${locationName}`
        + `&appid=${API.KEY}`

}





const createCoordinateBasedQueryURI = (coordinate, feature = "weather") => {
    return `${API.URI(feature)}`
        + `lat=${coordinate.latitude}`
        + `&lon=${coordinate.longitude}`
        + `&appid=${API.KEY}`;
}


const createDeepClone = (data) => {
    return JSON.parse(JSON.stringify(data));
}





async function getWeatherData(queryURI) {
    if (cache.data.queryURI === queryURI) return cache.data;
    let response = await fetch(queryURI);
    let data = await response.json();
    cache.data = data;
    cache.data.queryURI = queryURI;
    return data
}


const convertKtoF = (k) => {
    return (k - 273.16) * 1.8 + 32.0;
}


function roundTranc(number, digitsAfterDecimal) {

    const digitFactor = (10 ** digitsAfterDecimal);
    const rounded = (Math.round(number * digitFactor)) / digitFactor;

    console.log(number, digitsAfterDecimal, digitFactor, rounded);
    return rounded;
}


function toCelsiusData(data) {
    let convertedData = createDeepClone(data);
    convertedData.main.temp_max = roundTranc(convertKtoC(data.main.temp_max), 2) + " C";
    convertedData.main.temp_min = roundTranc(convertKtoC(data.main.temp_min), 2) + " C";
    convertedData.main.temp = roundTranc(convertKtoC(data.main.temp), 2) + " C";
    return convertedData;
}


function convertCtoF(c) {
    return c * 1.8 + 32;
}


function convertFtoC(f) {
    return (f - 32) * (5 / 9);
}

function convertKtoC(k) {
    return k - 273.16;
}


function extractNumber(str) {
    return str.replace(/[^0-9\.]+/g, "");
}

function convertViewTo(to, convert) {
    if (temperatureElement.type === to) return;
    const hello = temperatureElement.generatedElements.concat(temperatureElement.defaultElements);
    
    hello.forEach(id => {
        const current = document.querySelector(`#${id}`).innerText;
        const currentNumber = extractNumber(current);
        const converted = convert(currentNumber);
        const truncated = roundTranc(converted, 2);


        document.querySelector(`#${id}`).innerText = truncated + " " + to;
    })
    temperatureElement.type = to;
}


const convertFViewToC = () => {
    convertViewTo("C", convertFtoC);
}


const convertCViewToF = () => {
    convertViewTo("F", convertCtoF);
}


const convertKViewToC = () => {
    convertViewTo("C", convertKtoC);
}




function toFarenheitData(data) {
    let convertedData = createDeepClone(data);
    convertedData.main.temp_max = roundTranc(convertKtoF(data.main.temp_max), 2) + " F";
    convertedData.main.temp_min = roundTranc(convertKtoF(data.main.temp_min), 2) + " F";
    convertedData.main.temp = roundTranc(convertKtoF(data.main.temp), 2) + " F";
    return convertedData;
}


async function update3HourlyView(param) {

    function createSmallView(weatherData, index){
        const div = document.createElement("div");
        div.classList.add("with-border");
        
        div.innerHTML = `
        <div class="time">${new Date(parseInt(weatherData.dt + "000")).toLocaleString()}</div>
        <div id="t-${index}">${weatherData.main.temp}</div>
        
        `;
        temperatureElement.generatedElements.push(`t-${index}`);
        document.getElementById("sm").appendChild(div);
        

    }

    temperatureElement.clearGenerated();
    document.getElementById("sm") === null? true: document.getElementById("sm").remove();
    const sm = document.createElement("div");
    sm.id="sm";
    document.getElementById("sm-container").appendChild(sm);
   

    const uri = createCoordinateBasedQueryURI({ latitude: param.coord.lat, longitude: param.coord.lon }, "forecast");
    const data = await getWeatherData(uri);
    data.list.forEach((weatherData, index)=>{
        createSmallView(weatherData,index)
        
    })

   



}




async function updateWeatherView(data) {

    document.querySelector("#location").innerText = data.name;
    document.querySelector("#temp").innerText = data.main.temp;

    document.querySelector("#max-temp").innerText = data.main.temp_max;
    document.querySelector("#min-temp").innerText = data.main.temp_min;
    document.querySelector("#icon").src = API.IMG(data.weather[0].icon);
    document.querySelector("#humidity").innerText = data.main.humidity;
    document.querySelector("#description").innerText = data.weather[0].description;
    document.querySelector("#windSpeed").innerText = data.wind.speed;
    document.querySelector("#windGust").innerText = data.wind.gust;
    document.querySelector("#windDeg").innerText = data.wind.deg;
    await update3HourlyView(data);
    convertKViewToC();
   




}


const getGeoLocation = () => {
    return new Promise((resolve, reject) => {
        navigator.geolocation.getCurrentPosition((locationObject) => {
            resolve(locationObject.coords);
        });
    });
}



(async () => {

    const coordinate = await getGeoLocation();
    const uri = createCoordinateBasedQueryURI(coordinate);



    const data = await getWeatherData(uri);
    console.log(coordinate, uri, data);
    updateWeatherView(data);





})();
// updateWeatherView(await getWeatherData(createLocationQueryURI("Dhaka")));
