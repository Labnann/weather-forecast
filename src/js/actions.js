async function handleShowButtonClick(){
    const countrySelection = document.querySelector("#country-select");
    const location = countrySelection.selectedOptions[0].value;
    const queryUri = createLocationQueryURI(location);
    const data = await getWeatherData(queryUri);
    temperatureElement.type="K";
    updateWeatherView(data);
    
}

function handleChange() {
    if(temperatureElement.type==="C") convertCViewToF();
    else convertFViewToC(); 

    
}


const handleToCClick = ()=>{
    
    convertCViewToF();
    console.log(this);
}

const handleToFClick = () =>{
    convertFViewToC();
}


document.getElementById("button-show").onclick = handleShowButtonClick;
document.getElementById("button-change").onclick = handleChange;




